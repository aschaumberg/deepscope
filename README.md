DeepScope
=========

This tool applies saliency annotations to whole slide regions and is trained using pathologists diagnosing cancer at the microscope.

Please see https://doi.org/10.1101/097246 for more information.

Currently, this repository provides the trained Caffe models that make the predictions in bladder cancer (transurethral resection) and prostate cancer (needle biopsy).
